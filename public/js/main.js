(function() {

  var APP = angular.module("myAPP",['angular-loading-bar', 'angular-carousel']);

    APP.controller("MainController",function($scope,$location) {
        $scope.selectLang = {
            'anglais' :{
                trad : 'Select your language'
            },
            'francais' :{
                trad : 'Selectionner votre langue'
            },
            'chinoi' :{
                trad : 'tching ping'
            }
        }

        $scope.lang = 'francais';
        $scope.title = $scope.selectLang[$scope.lang].trad;

        $scope.$watch( "lang", function( newValue ) {
            $scope.title = $scope.selectLang[$scope.lang].trad;
        });
    });
})();
