var config  = require('./config.json');
var express = require('express');
var app     = express();
var path    = require('path');

app.set('views', path.join(__dirname, 'views') );
app.set('view engine', 'jade');
app.use( express.static( path.join(__dirname, 'public') ) );

app.get('*',function(req,res) {
  res.render('layout');
});

var server = app.listen(process.env.PORT || config.port, process.env.IP || config.ip);
